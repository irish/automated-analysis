#!/usr/bin/env python2.7
"""Automatically generate web-pages and graphs from logs using CERN's ROOT
(http://root.cern.ch) and vault_ana.py.  Makes use of scp, so key-based
authentication (or similar) should be configured to access the logs
automatically (i.e.: no password or passphrase).  By default, it will create
plots for the past week's worth of logs (one week ago to yesterday) for all
assets listed in AutoAna.conf. Attempts to preserve bandwidth by not
downloading tarballs of log files which already exist on the local system, and
only downloads the uncompressed log files when the tarball does _not_ exist on
the remote system.

Default invocation:
	If no arguments are given, it will perform as if called as:

	./AutoAna.py --config /etc/AutoAna.conf --start-date <1_WEEK_AGO> --end-date <YESTERDAY>

--config argument:
	If used with the --config argument, the specified configuration file will be
	used. All sections and options are specified in the included AutoAna.conf.

	This file should be of the form:

		[Section]
		Option = Value

	Monitored systems should be added to the Assets section.

		[Assets]
		ASSET_TAG_1 = IP_ADDRESS_1 HOSTNAME_1
		ASSET_TAG_2 = IP_ADDRESS_2 HOSTNAME_2
		...
		ASSET_TAG_N = IP_ADDRESS_N HOSTNAME_N

	Separate values with whitespace and assets with new lines.

	Note: The HOSTNAME is used only to label the reports, not for network
	resolution, but the IP_ADDRESS may be a fully-qualified domain name.

--start-date argument:
	If used with the --start-date argument, the date provided in %Y%m%d format
	represents the first day of logs to retrieve. March 14, 2013 would be
	20130314. If not used, the start date is assumed to be one week ago.

--end-date argument:
	If used with the --end-date argument, the date provided in %Y%m%d format
	represents the last day of logs to retrieve. March 14, 2013 would be
	20130314. If not used, the end date is assumed to be yesterday.

--log argument:
	If used with the --log argument, the specified logging level is enabled.
	Valid options are: DEBUG, INFO, WARNING, ERROR, and CRITICAL.
"""
import argparse
import ConfigParser
import datetime
import logging
import os.path
import re
import subprocess
import tarfile

def date_range(start_date, end_date):
	"""Generate a range of dates from two datetimes, inclusive."""
	for days in range(int((end_date - start_date).days) + 1):
		yield start_date + datetime.timedelta(days)


class AutoAna:
	"""Retrieves logs for assets and dispatches plot generation."""
	def __init__(self, config='/etc/AutoAna.conf',
			start_date=datetime.datetime.today() - datetime.timedelta(7),
			end_date=datetime.datetime.today() - datetime.timedelta(1),
			log_level='WARNING'):
		self.assets = {}
		self.config = config
		self.start_date = start_date
		self.end_date = end_date

		# Read config file
		config_parser = ConfigParser.SafeConfigParser()
		config_parser.read(config)

		# Configure logging
		logging.basicConfig(filename=config_parser.get("Analysis", "AutoAna Log"),
				filemode='w', level=log_level)
		logging.info(config_parser.items("Assets"))
		logging.debug(self)

		# Sanity check dates
		if start_date > end_date:
			error = "End date is before start date. Aborting."
			logging.error(error)
			raise SystemExit(error)

		# Add assets to dictionary
		for asset in config_parser.options("Assets"):
			asset_ip, asset_hostname = \
					str(config_parser.get("Assets", asset)).split()
			self.assets[asset] = {'ip': asset_ip,
					'hostname': asset_hostname}

		# Store some configuration options for easy access
		self.analysis = {}
		self.analysis['date_format'] = config_parser.get("Analysis", "Date Format")
		self.analysis['tgzs_path'] = config_parser.get("Analysis", "TGZs")
		self.analysis['logs_path'] = config_parser.get("Analysis", "Logs")
		self.analysis['png_path'] = config_parser.get("Analysis", "PNGs")
		self.analysis['www_path'] = config_parser.get("Analysis", "WWW")
		self.analysis['asset_config'] = config_parser.get("Analysis", "Asset Config")

		logging.debug(self)

	def retrieve_logs(self, asset):
		"""Retrieve log files for a given asset between start_date and end_date."""
		for date in date_range(self.start_date, self.end_date):
			date_str = date.strftime(self.analysis['date_format'])
			file_name = "%s_%s.tgz" % (asset, date_str)
			file_path = "%s/%s" % (self.analysis['tgzs_path'], file_name)
			if not os.path.isfile(file_path):
				command = "scp root@%s:/root/PerfMon/%s %s" % \
						(self.assets[asset]['ip'], file_name,
						self.analysis['tgzs_path'])
				logging.info(command)
				try:
					subprocess.check_call(command, shell=True)
				except subprocess.CalledProcessError as exception:
					logging.error(exception)
					logging.warning("TGZ not found, attempting to download raw logs.")
					file_name = "%s_%s_{Dark,IO}Mon.log" % (asset, date_str)
					command = "scp root@%s:/root/PerfMon/%s %s" % \
							(self.assets[asset]['ip'], file_name,
							self.analysis['logs_path'])
					logging.info(command)
					try:
						subprocess.check_call(command, shell=True)
					except subprocess.CalledProcessError as exception:
						logging.error(exception)

		# Logging
		logging.debug("Asset: " + asset)
		logging.debug("IP: " + self.assets[asset]['ip'])
		logging.debug("Hostname: " + self.assets[asset]['hostname'])

	def retrieve_all_logs(self):
		"""Retrieve log files for all assets between start_date and end_date."""
		for asset in self.assets:
			self.retrieve_logs(asset)

	def extract_logs(self, file_name):
		"""Attempt to extract the specified file to self.analysis['logs_path']"""
		logging.info("Extracting: " + file_name)
		try:
			with tarfile.open(file_name) as tar:
				for member in tar.getmembers():
					member.name = os.path.basename(member.name)
				tar.extractall(self.analysis['logs_path'])
		except (IOError, tarfile.TarError) as exception:
			logging.error(exception)

	def update_plot_config(self, asset):
		"""Update the plot config file to include the object's date range. Plot
		config files are named 'config_ASSET_HOSTNAME."""
		file_path = "%s/config_%s_%s" % \
				(self.analysis['asset_config'], asset,
				self.assets[asset]['hostname'])
		date_expression = re.compile(r'^Dates\s+\d{8}')

		# Read file contents, write all but dates, add new dates
		with open(file_path, "r+") as file_handle:
			logging.info(file_handle)
			lines = file_handle.readlines()
			file_handle.seek(0)
			file_handle.truncate()

			for line in lines:
				if not re.match(date_expression, line):
					logging.debug(line.strip())
					file_handle.write(line)

			# Insert dates for each date in date_range(start_date, end_date)
			for date in date_range(self.start_date, self.end_date):
				logging.debug("Adding Date: " + \
						date.strftime(self.analysis['date_format']))
				file_handle.write("Dates %s\n" % \
						date.strftime(self.analysis['date_format']))

	def copy_image(self, asset):
		"""Copy the generated plot to self.analysis['www_path']/ASSET. Rename to
		ASSET_START_DATE_END_DATE.png"""
		# Construct file paths
		# Example: ./png/833-100-00057_20130807.png
		source = "%s/%s_%s.png" % (self.analysis['png_path'], asset,
				self.start_date.strftime(self.analysis['date_format']))
		# Example: /srv/www/plots/833-100-00057/833-100-00057_20130807_20130814.png
		destination = "%s/%s/%s_%s_%s.png" % (self.analysis['www_path'],
				asset, asset, self.start_date.strftime(self.analysis['date_format']),
				self.end_date.strftime(self.analysis['date_format']))
		logging.debug("Source: " + source)
		logging.debug("Destination: " + destination)

		try: # Ensure directory structure
			command = "mkdir -p %s/%s" % (self.analysis['www_path'], asset)
			logging.info(command)
			subprocess.check_call(command, shell=True)
		except subprocess.CalledProcessError as exception:
			logging.error(exception)

		try: # Copy source to destination
			command = "cp %s %s" % (source, destination)
			logging.info(command)
			subprocess.check_call(command, shell=True)
		except subprocess.CalledProcessError as exception:
			logging.error(exception)

	def plot_asset(self, asset):
		"""Generate plot for a given asset."""
		# Extract logs from *.tgz
		for date in date_range(self.start_date, self.end_date):
			tar_name = "%s/%s_%s.tgz" % \
					(self.analysis['tgzs_path'], asset,
					date.strftime(self.analysis['date_format']))
			self.extract_logs(tar_name)

		self.update_plot_config(asset)

		try: # Generate histograms
			command = "./run.py -a %s" % asset
			subprocess.check_call(command, shell=True)
			logging.info(command)
			try: # Generate PNGs
				command = "./run.py -a %s -p" % asset
				subprocess.check_call(command, shell=True)
				logging.info(command)
			except subprocess.CalledProcessError as exception:
				logging.error(exception)
		except subprocess.CalledProcessError as exception:
			logging.error(exception)

		self.copy_image(asset)

	def plot_all_assets(self):
		"""Generate all plots."""
		# Retrieve logs
		self.retrieve_all_logs()

		# Plot individual assets
		for asset in self.assets:
			self.plot_asset(asset)

	def __str__(self):
		return "Config: " + str(self.config) + "\n" + \
				"Start Date: " + str(self.start_date) + "\n" + \
				"End Date: " + str(self.end_date) + "\n" + \
				"Date Format: " + str(self.analysis['date_format']) + "\n" + \
				"TGZs: " + str(self.analysis['tgzs_path']) + "\n" + \
				"Logs: " + str(self.analysis['logs_path']) + "\n" + \
				"Assets: " + str(self.assets)


def main():
	"""Run if not imported."""
	yesterday = (datetime.datetime.today() - \
			datetime.timedelta(1)).strftime("%Y%m%d")
	last_week = (datetime.datetime.today() - \
			datetime.timedelta(7)).strftime("%Y%m%d")

	# Argument parsing
	parser = argparse.ArgumentParser(description=
			'Automatically generate performance graphs')
	parser.add_argument('--config', '-c', metavar='CONFIG_FILE', type=str,
			action='store', dest='CONFIG', default='/etc/AutoAna.conf',
			help='Configuration file to use')
	parser.add_argument('--start-date', '-s', metavar='START_DATE',
			action='store', dest='START_DATE', default=last_week,
			help='The earliest date for which to collect logs in Ymd format')
	parser.add_argument('--end-date', '-e', metavar='END_DATE',
			action='store', dest='END_DATE', default=yesterday,
			help='The latest date for which to collect logs in Ymd format')
	parser.add_argument('--log', '-l', metavar='LOG_LEVEL',
			action='store', dest='LOG_LEVEL', default='WARNING',
			help='Sets the log level (DEBUG, INFO, WARNING, ERROR, CRITICAL)')
	parser.add_argument('--version', action='version', version='%(prog)s 0.9.3')

	args = parser.parse_args()
	start_date = datetime.datetime.strptime(args.START_DATE, "%Y%m%d")
	end_date = datetime.datetime.strptime(args.END_DATE, "%Y%m%d")

	# Retrieve logs
	auto_ana = AutoAna(args.CONFIG, start_date, end_date, args.LOG_LEVEL)
	auto_ana.plot_all_assets()

if __name__ == "__main__":
	main()
